/*
Copyright IBM Corp. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"os"

	"gitee.com/bupt-zkjc/fabric/bccsp/factory"
	"gitee.com/bupt-zkjc/fabric/cmd/common"
	discovery "gitee.com/bupt-zkjc/fabric/discovery/cmd"
)

func main() {
	factory.InitFactories(nil)
	cli := common.NewCLI("discover", "Command line client for fabric discovery service")
	discovery.AddCommands(cli)
	cli.Run(os.Args[1:])
}
